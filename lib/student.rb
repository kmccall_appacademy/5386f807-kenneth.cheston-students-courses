class Student

  attr_reader :student, :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
    @course_load = {}
  end

  def name
    @first_name + " " + @last_name
  end

  def enroll(new_course)
    raise "course would conflict" if courses.any? { |course| new_course.conflicts_with?(course) }
    self.courses << new_course unless @courses.include?(new_course)
    new_course.students << self
  end

  def course_load
    @courses.each do |course|
      if @course_load.include?(course.department)
        @course_load[course.department] += course.credits
      else @course_load[course.department] = course.credits
      end
    end
    @course_load
  end
end
